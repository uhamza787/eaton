import { View, StyleSheet } from "react-native";
import React from "react";
import { Header, Button, Input } from "../components";

export default function Login() {
  return (
    <View>
      <Header headingText={"Login"} />
      <View style={styles.form}>
      <Input inputTitle={'email'} error = {false} valid={true} />
      <Input inputTitle={'password'} isSecure={true} error={true}/>
      <View style={styles.buttonCon}>
        <Button text={"hellow"} onBtnPress={()=>{alert('hello')}} />
      </View>
    </View>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonCon: {
    height: 45,
    width: "20%",
  },
  form:{
    padding:10
  }
});
